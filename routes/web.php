<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/','comidas');

Auth::routes();

Route::get('comidas', 'Web\PageController@index')->name('index');
Route::resource('cart', 'Web\CartController');
Route::resource('admin/carts/cancelar','Admin\Cart\CancelarController');
Route::resource('admin/carts/procesando','Admin\Cart\ProcesandoController');
Route::resource('admin/carts/preparando','Admin\Cart\PreparandoController');
Route::resource('admin/carts/listo','Admin\Cart\ListoController');
Route::resource('admin/carts/entregado','Admin\Cart\EntregadoController');
Route::resource('admin/foods', 'Admin\FoodController');
Route::resource('admin/users', 'Admin\UserController');
Route::resource('admin/carts', 'Admin\CartController');
Route::get('admin/', 'Web\PageController@admin')->name('admin');