<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       factory(App\User::class, 29)->create();
        App\User::create([
        	'name' => 'Alejandro',
            'email' => 'alexparralesrmz@outlook.com',
            'phone' => '4612392559',
            'status' => 'ACTIVO',
            'role' => 'ADMIN',
            'password' => bcrypt('123'),
        ]);
    }
}
