<?php

use Faker\Generator as Faker;

$factory->define(App\Cart::class, function (Faker $faker) {
    return [
        'user_id' => rand(1,30),
        'food_id' => rand(1,24),
        'status' => $faker->randomElement(['PROCESANDO', 'PREPARANDO','LISTO','ENTREGADO','CANCELADO']),
    ];
});
