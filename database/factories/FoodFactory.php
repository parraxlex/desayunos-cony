<?php

use Faker\Generator as Faker;

$factory->define(App\Food::class, function (Faker $faker) {
    $title = $faker->sentence(4);
    return [
        'name' => $title,
        'slug' => str_slug($title),
        'description' => $faker->text(200),
        'price' => rand(30,150),
        'file' => $faker->imageUrl($wiDth = 400, $height = 400),
        'status' => $faker->randomElement(['VISIBLE', 'NO-VISIBLE']),
    ];
});
