@section('title')
Fonda Cony: Todas la comidas
@endsection

@extends('layouts.app')

@section('content')

<body>

  <div class="page-header header-filter header-small" data-parallax="true" style="background-image: url('{{ asset('img/cover.jpg') }}');">
    <div class="container">
      <div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="brand">
						<h1 class="title text-center">Comidas del dia de hoy 
              
            </h1>
						
					</div>
				</div>
			</div>
		</div>
	</div>

  <div class="main main-raised">
    <div class="section">
      <div class="container">
        


        <div class="row">
          <div class="col-md-12">
          <h2 class="text-center"><script>
              var meses = new Array ("Enero","Febrero","Marzo","Abril","mayo","junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
              var diasSemana = new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
              var f=new Date();
              document.write(diasSemana[f.getDay()] + " " + f.getDate() + " de " + meses[f.getMonth()] );
              </script></h2>
            </div>
        </div>
        <br><br><br>
             

        <?php
        $numOfCols = 3;
        $rowCount = 0;
        $bootstrapColWidth = 12 / $numOfCols;
        ?>
        <div class="row">
          @foreach($foods as $food)
          
          
        
         
          <div class="col-md-<?php echo $bootstrapColWidth; ?> wow animated bounceIn">
                  <div class="card card-profile">
                    <div class="card-image">
                      <a href="">
                        <img class="img" src="{{ $food->file }}">

                        <div class="card-title">
                          {{ $food->name }}
                        </div>
                      </a>
                   </div>

                    <div class="card-content">
                      <a class="btn btn-danger btn-round">${{ $food->price }}</a>

                      <p class="card-description">
                        {{ $food->description }}
                      </p>

                      @guest

                      <div class="footer">

                        <a href="{{ route('login') }}" class="btn btn-warning ">Añadir al pedido &nbsp;<li class="fa fa-cart-plus"></li></a>
                      </div>
                      @else


                      <div class="footer">
                       {!! Form::open(['route'=>'cart.store'])!!}
                       {!! Form::hidden('food_id', $food->id) !!}
                       {!! Form::hidden('user_id', auth()->user()->id) !!}
                        <button class="btn btn-warning">Añadir al pedido &nbsp;<li class="fa fa-cart-plus"></li></button>

                      {!! Form::close() !!}
                      </div>
                      @endguest

                    </div>
                  </div>
              
                </div>
                <?php
    $rowCount++;
    if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
              ?>
            @endforeach
      </div>
    </div><!-- section -->


	</div> <!-- end-main-raised -->


</body>
@endsection