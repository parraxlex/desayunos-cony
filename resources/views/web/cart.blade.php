@section('title')
Fonda Cony: Todos los pedidos
@endsection



@extends('layouts.app')

@section('content')
<body>
  <div class="page-header header-filter header-small" data-parallax="true" style="background-image: url('{{ asset('img/cover.jpg') }}');">
    <div class="container">
      <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="brand">
                        <h1 class="title text-center">Todos los pedidos de {{ $user->name }}</h1>
                        <h2 class="text-center"></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="main main-raised">

<div class="container">
<div class="col-md-8 col-md-offset-2">
    <div class="table-responsive">
        <br><br>
    <table class="table">
        <thead>
            <tr>
                <th class="text-center">#</th>
                <th>Comida</th>
                <th>Estatus</th>
                <th>Fecha</th>
                <th>Ultima actualizacion</th>
                <th class="text-right">Acciones</th>
            </tr>
        </thead>
        <tbody>

        	@foreach($carts as $cart)
            <tr>
                <td class="text-center">{{ $cart->id }}</td>
                <td>{{ $cart->food_id }}</td>
                <td>{{ $cart->status }}</td>
                <td>{{ $cart->created_at }}</td>
                <td>{{ $cart->updated_at->diffForHumans() }}</td>
                <td class="td-actions text-right">
                @if( $cart->status == "PROCESANDO")
                
                   
                   {!! Form::model($cart, ['route' => ['cancelar.update', $cart->id], 'method' => 'PUT', 'style'=>'display:inline-block;']) !!}
                       {!! Form::hidden('status', 'CANCELADO') !!}

                         <button rel="tooltip" class="btn btn-danger" data-original-title="Cancelar pedido" title="">
                            <i class="material-icons">close</i>
                        </button>

                         {!! Form::close() !!}
               
                @endif
                 </td>


            </tr>
            @endforeach


  
         
        </tbody>
    </table>

       {{ $carts->render() }}

    </div>



</div>
</div>
</div>

</body>
@endsection