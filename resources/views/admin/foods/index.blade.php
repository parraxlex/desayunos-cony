@if(Auth::user()->role == "ADMIN")

@section('title')
Fonda Cony: Todas las comidas
@endsection

@extends('layouts.app')

@section('content')

<body>
<div class="page-header header-filter header-small" data-parallax="true" style="background-image: url('{{ asset('img/cover.jpg') }}');">
    <div class="container">
      <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="brand">
                        <h1 class="title text-center">Todas las comidas</h1>
                        <h2 class="text-center"></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>









<div class="main main-raised">

    <div class="container">
<br><br>
<div class="row">
	<div class="col-md-12">
		<a href="{{ route ('foods.create') }}" class="btn btn-warning"><i class="material-icons">
			add
		</i>&nbsp; Crear nueva</a>
	</div>
</div>


        <div class="col-md-12">
            <div class="table-responsive">
                <br><br>
                <table class="table">
                    <thead>
						<tr>
							<th width="10px">ID</th>
							<th>Imagen</th>
							<th>Nombre</th>
							<th>Estado</th>
							<th class="text-right">Acciones</th>
						</tr>
					</thead>
					<tbody>
						@foreach($foods as $food)
						<tr>
							<td>{{ $food->id}}</td>
							<td>
								<div class="img-container">
		                        	<img src="{{ $food->file }}" width="50px" height="auto">
		                        </div>
		                    </td>
							<td>{{ $food->name}}</td>
							<td>{{ $food->status}}</td>
							<td class="td-actions text-right">
							<a href="{{ route('foods.edit', $food->id) }}" class="btn btn-sm btn-warning"><i class="material-icons">edit</i></a>
							{!! Form::open(['route' => ['foods.destroy', $food->id], 'method' => 'DELETE',  'style'=>'display: inline-block;'])!!}
								<button class="btn btn-danger"><i class="material-icons">restore_from_trash</i></button>
							{!! Form::close() !!}
							</td>

						</tr>
						@endforeach
					</tbody>
    </table>

    {{ $foods->render() }}

</div>



</div>
</div>
</div>

</body>
@endsection
@endif