@if(Auth::user()->role == "ADMIN")

@section('title')
Fonda Cony: Todas las comidas
@endsection

@extends('layouts.app')

@section('content')

<body>

	<div class="page-header header-filter header-small" data-parallax="true" style="background-image: url('{{ asset('img/cover.jpg') }}');">
    <div class="container">
      <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="brand">
                        <h1 class="title text-center">Editar comida</h1>
                        <h2 class="text-center"></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>


	<div class="main main-raised">

    <div class="container">
        <div class="col-md-12">
		<br><br><br>	
					

			{!! Form::model($food, ['route' => ['foods.update', $food->id], 'method' => 'PUT', 'files' => true]) !!}
				@include('admin.foods.partials.form')
			{!! Form::close() !!}

	
			</div>
		</div>
		
	</div>
</body>

@endsection
@endif