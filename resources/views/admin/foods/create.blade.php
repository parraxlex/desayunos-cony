@if(Auth::user()->role == "ADMIN")

@section('title')
Fonda Cony: Crear comida
@endsection

@extends('layouts.app')

@section('content')




<body>
	<div class="page-header header-filter header-small" data-parallax="true" style="background-image: url('{{ asset('img/cover.jpg') }}');">
    <div class="container">
      <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="brand">
                        <h1 class="title text-center">Crear nueva comida</h1>
                        <h2 class="text-center"></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>


<div class="main main-raised">
	<div class="container">
		<br><br>
		{!! Form::open(['route'=>'foods.store','files' => true])!!}
			@include('admin.foods.partials.form')
		{!! Form::close() !!}
	</div>
</div>


</body>
@endsection
@endif