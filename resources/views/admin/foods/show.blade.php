@extends('layouts.app')


@section('content')

<div class="container">
	<div class="row">
		<div class="col-8 offset-2">
			<div class="card ">
				<div class="card-heading">
					<a href="{{ route('categories.edit', $category->id) }}" class="btn btn-sm btn-primary float-right">Editar</a>
				</div>
				<div class="card-body">
					<p><strong>Nombre</strong> {{ $category->name }}</p>
					<p><strong>Slug</strong> {{ $category->slug }}</p>
				</div>
			</div>
		</div>
	</div>
	
</div>

@endsection