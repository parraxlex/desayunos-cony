<div class="row">
	<div class="col-md-12">
		<a href="{{ route ('foods.index') }}" class="btn btn-warning"><i class="material-icons">
			keyboard_backspace
		</i>&nbsp; Regresar</a>
	</div>
</div>


<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			{{ Form::label('name', 'Nombre de la comida') }}
			{{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
		</div>
	</div>
	<div class="col-md-6">
	<div class="form-group">
		{{ Form::label('slug', 'URL amigable') }}
		{{ Form::text('slug', null, ['class' => 'form-control', 'id' => 'slug']) }}
	</div>
	</div>
</div>
<div class="row">
		<div class="col-md-6">
	<div class="form-group">
		{{ Form::label('description', 'Descripcion') }}
		{{ Form::text('description', null, ['class' => 'form-control', 'id' => 'description']) }}
	</div>
	</div>
	<div class="col-md-6">
	<div class="form-group">
		{{ Form::label('price', 'Precio') }}
		{{ Form::text('price', null, ['class' => 'form-control', 'id' => 'price']) }}
	</div>
	</div>
</div>




<div class="row">


	<div class="col-md-6">
		<h4><small>Regular Image</small></h4>
		<div class="fileinput fileinput-new text-center" data-provides="fileinput">
			<div class="fileinput-new thumbnail img-raised">
				<img src="assets/img/image_placeholder.jpg" alt="...">
			</div>
			<div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
			<div>
				<span class="btn btn-raised btn-round btn-default btn-file">
					<span class="fileinput-new">Seleccionar imagen</span>
					<span class="fileinput-exists">Cambiar</span>
					{{ Form::file('file', null) }}
				</span>
				<a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Eliminar</a>
			</div>
		</div>
	</div>

<div class="col-md-6">
<div class="form-group">
	{{ Form::label('status', 'Estado') }}



	<div class="radio">
		<label>
			{{ Form::radio('status', 'VISIBLE') }} <span class="circle"></span><span class="check"></span> Visible
		</label>
	</div>
	<div class="radio">
	<label>
		{{ Form::radio('status', 'NO-VISIBLE') }}<span class="circle"></span><span class="check"></span> No visible
	</label>
	</div>
</div>
</div>
</div>
<div class="row">
	<div class="col-md-12">

	<div class="form-group text-center">
		{{ Form::submit('Guardar', ['class' => 'btn btn-success']) }}
	</div>
</div>
</div>

@section('scripts')
<script type="text/javascript" src="{{ asset('js/jquery.stringToSlug.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
	    $("#name, #slug").stringToSlug({
	        callback: function(text){
	            $('#slug').val(text);
	        }
	    });
	});
</script>
@endsection
