<div class="row">
	<div class="col-md-12">
		<a href="{{ route ('users.index') }}" class="btn btn-warning"><i class="material-icons">
			keyboard_backspace
		</i>&nbsp; Regresar</a>
	</div>
</div>



<div class="row">
<div class="col-md-6">
<div class="form-group">
	{{ Form::label('name', 'Nombre del usuario') }}
	{{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
</div>
</div>
<div class="col-md-6">
<div class="form-group">
	{{ Form::label('email', 'Email') }}
	{{ Form::text('email', null, ['class' => 'form-control', 'id' => 'email']) }}
</div>
</div>
</div>


<div class="row">
<div class="col-md-6">
<div class="form-group">
	{{ Form::label('phone', 'Telefono') }}
	{{ Form::text('phone', null, ['class' => 'form-control', 'id' => 'phone']) }}
</div>
</div>
<div class="col-md-6">
<div class="form-group">
	{{ Form::label('status', 'Estado') }}

	<div class="radio">
		<label>
			{{ Form::radio('status', 'ACTIVO') }} <span class="circle"></span><span class="check"></span> Activo
		</label>
	</div>


	<div class="radio">
		<label>
			{{ Form::radio('status', 'INACTIVO') }} <span class="circle"></span><span class="check"></span> Inactivo
		</label>
	</div>

</div>
</div>
</div>

<div class="row">
<div class="col-md-6">
<div class="form-group">
	{{ Form::label('role', 'Rol') }}

	<div class="radio">
		<label>
			{{ Form::radio('role', 'ADMIN') }} <span class="circle"></span><span class="check"></span> Administrador
		</label>
	</div>


	<div class="radio">
		<label>
			{{ Form::radio('role', 'USER') }} <span class="circle"></span><span class="check"></span> Usuario
		</label>
	</div>

</div>
</div>
<div class="col-md-6">
<div class="form-group">
	{{ Form::submit('Guardar', ['class' => 'btn btn-success']) }}
</div>
</div>
</div>




@section('scripts')
<script type="text/javascript" src="{{ asset('js/jquery.stringToSlug.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
	    $("#name, #slug").stringToSlug({
	        callback: function(text){
	            $('#slug').val(text);
	        }
	    });
	});
</script>
@endsection
