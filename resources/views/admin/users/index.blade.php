@if(Auth::user()->role == "ADMIN")

@section('title')
Fonda Cony: Todas las comidas
@endsection

@extends('layouts.app')

@section('content')

<body>

	<div class="page-header header-filter header-small" data-parallax="true" style="background-image: url('{{ asset('img/cover.jpg') }}');">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="brand">
						<h1 class="title text-center">Editar usuario</h1>
						<h2 class="text-center"></h2>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="main main-raised">



    <div class="container">
        <div class="col-md-12">
            <div class="table-responsive">
                <br><br>
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Nombre</th>
                            <th>Estatus</th>
                            <th>Email</th>
                            <th>Telefono</th>
                            <th>Ultimo Acceso</th>
                            <th class="text-right">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>

                    	@foreach($users as $user)
                    	<tr>
                    		<td>{{ $user->id}}</td>
                    		<td>{{ $user->name}}</td>
                    		<td>{{ $user->status}}</td>
                    		<td>{{ $user->email }}</td>
                    		<td>{{ $user->phone}}</td>
                    		<td>{{ $user->updated_at}}</td>
                    		<td class="td-actions text-right">
                    		<a href="{{ route('users.edit', $user->id) }}" class="btn btn-warning"><i class="material-icons">edit</i></a>
                    		{!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'DELETE', 'style'=>'display:inline-block;'])!!}
                    			<button class="btn btn-sm btn-danger">
                    				<i class="material-icons">restore_from_trash</i>
                    			</button>
                    			{!! Form::close() !!}
                    		</td>
                    	</tr>
            @endforeach




        </tbody>
    </table>

    {{ $users->render() }}

</div>



</div>
</div>

	</div>
</body>

@endsection
@endif