<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/apple-icon.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('img/favicon.png') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>@yield('title')</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/material-kit.css?v=1.2.1')}}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">

</head>


<nav class="navbar navbar-default navbar-transparent navbar-fixed-top navbar-color-on-scroll" color-on-scroll="100" id="sectionsNav">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('index') }}"><i class="material-icons">
restaurant
</i> &nbsp; Desayunos Cony</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
              
                @guest
                <li>
                    <a href="{{ route('register')}}">
                        <i class="material-icons">person_add</i> Registrate
                    </a>
                </li>
                <li class="button-container">
                    <a href="{{ route('login') }}" class="btn btn-danger btn-round">
                        <i class="material-icons">input</i> Entrar
                    </a>
                </li>
                @else
                @if(Auth::user()->role == "ADMIN")
                <li>
                    <a href="{{ route('admin') }}">
                        <i class="material-icons">settings</i> Administrador
                    </a>
                </li>
                @endif
                <li>
                    <a href="{{ route('cart.index') }}">
                        <i class="material-icons">shopping_cart</i> Mis pedidos
                    </a>
                </li>
                <li class="button-container">
                    <a class="btn btn-rose btn-round" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                      <i class="material-icons">clear</i> Salir
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                    </form>     
                </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>





@yield('content')

<br><br>
<footer class="footer footer-black footer-big">
    <div class="container">
        <div class="copyright pull-left">
            Acceso Sur #114, Plazas del Sol 3ra Sección CP 76099 Tel: (442) 452-1364 Santiago de Querétaro, Qro.
        </div>

        <div class="copyright pull-right">
            Copyright &copy; <script>document.write(new Date().getFullYear())</script> DESS All Rights Reserved.
        </div>
    </div>
</footer>





@if(session('info'))
<div class="modal fade in alert" aria-hidden="true" style="display: block; padding-right: 17px; background-color: rgba(0, 0, 0, 0.2);">
  <div class="modal-dialog modal-small ">
    <div class="modal-content">

      <div class="modal-body text-center">
        <h5>{{ session('info') }}</h5>
        <i class="material-icons" style="font-size: 40px;">check_circle</i>
      </div>
      <div class="modal-footer text-center">
        <button type="button" class="btn btn-success" data-dismiss="alert">Entendido!</button>
      </div>
    </div>
  </div>
</div>
@endif


@if(count($errors))
<div class="modal fade in alert" aria-hidden="true" style="display: block; padding-right: 17px; background-color: rgba(0, 0, 0, 0.2);">
  <div class="modal-dialog modal-small ">
    <div class="modal-content">

      <div class="modal-body text-center">
        <h5>@foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach</h5>
      </div>
      <div class="modal-footer text-center">
        <button type="button" class="btn btn-danger" data-dismiss="alert">Cerrar</button>
      </div>
    </div>
  </div>
</div>
@endif

<script src="{{ asset('js/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('js/wow.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/material.min.js') }}"></script>
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('js/nouislider.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap-datetimepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap-selectpicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap-tagsinput.js') }}"></script>
<script src="{{ asset('js/jasny-bootstrap.min.js') }}"></script>
<script src="{{ asset('js/material-kit.js?v=1.2.1') }}" type="text/javascript"></script>
<script>
    new WOW().init();
</script>
@yield('scripts')
</html>