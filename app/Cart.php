<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable= [
		'user_id', 'food_id', 'status'
	];

	public function user(){
		return $this->belongsTo(User::class);
	}

	public function foods(){
		return $this->belongsTo(Food::class);
	}
}
