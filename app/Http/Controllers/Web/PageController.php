<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Food;

class PageController extends Controller
{
    public function index()
    {
    	$foods = Food::orderBy('id','DESC')->where('status','VISIBLE')->get();
    	return view('web.index', compact('foods'));

    }
    public function contact()
    {
    	return view('web.contact');
    }
    public function admin()
    {
    	return view('admin.admin');
    }
}
