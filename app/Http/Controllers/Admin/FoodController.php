<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\FoodStoreRequest;
use App\Http\Requests\FoodUpdateRequest;
use App\Food;

class FoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */




    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $foods = Food::orderBy('id', 'DESC')->paginate(10);
        return view('admin.foods.index', compact('foods'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.foods.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FoodStoreRequest $request)
    {
        $food = Food::create($request->all());
        if($request->file('file')){
            $path = Storage::disk('public')->put('image',  $request->file('file'));
            $food->fill(['file' => asset($path)])->save();
        }

        return redirect()->route('foods.edit', $food->id)->with('info', 'Comida creada con éxito');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $food = Food::find($id);
        return view('admin.foods.edit', compact('food'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FoodUpdateRequest $request, $id)
    {
        $food = Food::find($id);
        $food->fill($request->all())->save();

        if($request->file('file')){
            $path = Storage::disk('public')->put('image',  $request->file('file'));
            $food->fill(['file' => asset($path)])->save();
        }

        return redirect()->route('foods.edit', $food->id)->with('info', 'Comida actualizada con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $food = Food::find($id)->delete();
        return back()->with('info', 'Comida eliminada correctamente');
    }
}



