<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    protected $fillable= [
		'name', 'slug', 'description','price','file','status'
	];

    public function foods(){
    	return $this->hasMany(Cart::class);
    }
}
